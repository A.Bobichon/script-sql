use woofing;

drop procedure if exists feedUser; 

DELIMITER // 
CREATE PROCEDURE feedUser(loop_int INT) 
BEGIN 

DECLARE i INT DEFAULT 0; 

TRUNCATE TABLE user;

WHILE (i <= loop_int) DO
		SET i = i+ 1;
   		INSERT INTO user ( nom, prenom ) 
		values (  
			CONCAT("nomUser", i),
            CONCAT("prenomUser", i)
			);
END WHILE; 
END; 
// 

