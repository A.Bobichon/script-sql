use woofing;

drop procedure if exists feedLieu; 

DELIMITER // 
CREATE PROCEDURE feedLieu(loop_int INT) 
BEGIN 

DECLARE i INT DEFAULT 0; 

TRUNCATE TABLE lieu;

WHILE (i <= loop_int) DO
		SET i = i+ 1;
   		INSERT INTO lieu ( description, capacite_accueil, capacite_prit, adresse) 
		values (  
			SUBSTRING(MD5(RAND()) FROM 1 FOR 15),
			ROUND(RAND() * 100),
			0,
			concat(RAND() * 10, " rue ", SUBSTRING(MD5(RAND()) FROM 1 FOR 8))
			);
END WHILE; 
END; 
// 

