const program = require('commander');
const mysql = require('mysql');
const fs = require('fs');

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "woofing"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });

program
    .command("RECREATE")
    .alias("RC")
    .description("recreate the table")

    .action( () => {
        con.connect( (err) => {
            if( err ) throw err;
            console.log("connected");

            const dataSql = fs.readFileSync('./callSQLWhitScript.sql').toString();
            con.query( dataSql, ( err, result ) => {
                if ( err ) throw err;
                console.log(" the script is executed !!");
            })
        } )
    })

program
    .command("TRUNCATE")
    .alias("T")
    .description("truncate all table")

    .action( () => {
        con.connect( (err) => {
            if( err ) throw err;
            console.log("connected");

            const dataSql = fs.readFileSync('./utilities/truncate_table.sql').toString();
            con.query( dataSql, ( err, result ) => {
                if ( err ) throw err;
                console.log(" the script is executed !!");
            })
        } )
    })

program
    .command("CREATE")
    .alias("C")
    .description("create the table")

    .action( () => {
        con.connect( (err) => {
            if( err ) throw err;
            console.log("connected");

            const dataSql = fs.readFileSync('./utilities/create.sql').toString();
            con.query( dataSql, ( err, result ) => {
                if ( err ) throw err;
                console.log(" the script is executed !!");
            })
        } )
    })


program.parse(process.argv);