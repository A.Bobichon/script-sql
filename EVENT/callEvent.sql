DROP EVENT IF EXISTS callFeeding;

DELIMITER // 
CREATE EVENT callFeeding  

ON SCHEDULE EVERY 1 MINUTE
STARTS CURRENT_TIMESTAMP

DO
	CALL truncate_table();
	CALL feedLieu(100);
	CALL feedUser(100);
//

