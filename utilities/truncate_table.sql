use woofing;

drop procedure if exists truncate_table; 

DELIMITER // 
CREATE PROCEDURE truncate_table() 

BEGIN
	
	TRUNCATE TABLE activite;
	TRUNCATE TABLE chantier;
	TRUNCATE TABLE chantier_act;
	TRUNCATE TABLE competence;
	TRUNCATE TABLE demande;
	TRUNCATE TABLE hote;
	TRUNCATE TABLE lieu;
	TRUNCATE TABLE presence;
	TRUNCATE TABLE ulieu;
	TRUNCATE TABLE user;
	TRUNCATE TABLE utilisateur;

END
//

call truncate_table();