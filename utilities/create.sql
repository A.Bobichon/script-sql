

-- Create the database
CREATE DATABASE IF NOT EXISTS woofing;

-- Create tables 
use woofing;

CREATE TABLE IF NOT EXISTS user(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	nom VARCHAR(100),
	prenom VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS lieu(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	description VARCHAR(500),
	capacite_accueil INT,
	capacite_prit INT,
	adresse VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS hote(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	idLieu INT REFERENCES lieu(id),
	CONSTRAINT LIEU_HOTE UNIQUE(idLieu),
	idUser INT REFERENCES user(id),
	CONSTRAINT USER_HOTE UNIQUE(idUser)
);

CREATE TABLE IF NOT EXISTS activite (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	type VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS utilisateur(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	idUser INT REFERENCES user(id),
	CONSTRAINT USER_HOTE UNIQUE(idUser)
);

CREATE TABLE IF NOT EXISTS competence (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	stars INT,
	typeId INT REFERENCES activite(id),
	idUser INT REFERENCES user(id)
);

CREATE TABLE IF NOT EXISTS demande(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,  
	validation BOOLEAN,
	idUtilisateur INT REFERENCES utilisateur(id),
	idHote INT REFERENCES hote(id)
);

CREATE TABLE IF NOT EXISTS ulieu(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	idLieu INT REFERENCES lieu(id),
	idVal INT REFERENCES validation(id)
);	

CREATE TABLE IF NOT EXISTS chantier(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	idLieu INT REFERENCES lieu(id),
	date_start DATETIME,
	date_end DATETIME,
	nb_need INT,
	nb_pres INT,
	etat BOOLEAN
);

CREATE TABLE IF NOT EXISTS chantier_act(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	idChantier INT REFERENCES chantier(id),
	idTypeAct INT REFERENCES activite(id)
);

CREATE TABLE IF NOT EXISTS presence(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	present BOOLEAN,
	idLieu  INT REFERENCES lieu(id),
	idChantier INT REFERENCES chantier(id)
);






