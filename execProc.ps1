[System.Reflection.Assembly]::LoadWithPartialName("MySql.Data")

[string]$sMySQLUserName = 'root'
[string]$sMySQLPW = 'root'
[string]$sMySQLDB = 'woofing'
[string]$sMySQLHost = 'localhost'
[string]$sConnectionString = "server="+$sMySQLHost+";port=3306;uid=" + $sMySQLUserName + ";pwd=" + $sMySQLPW + ";database="+$sMySQLDB 

$oConnection = New-Object MySql.Data.MySqlClient.MySqlConnection($sConnectionString)
$Error.Clear()
try
{
    $oConnection.Open()
    write-host "Connection opened"
}
catch
{
    write-warning ("Could not open a connection to Database $sMySQLDB on Host $sMySQLHost. Error: "+$Error[0].ToString())
}

#$oTransAction=$oConnection.BeginTransaction()
$oMYSQLCommand = New-Object MySql.Data.MySqlClient.MySqlCommand
$oMYSQLCommand.Connection=$oConnection


$sql = [io.file]::ReadAllText('C:\Users\alexa\Desktop\projetSQL - callSQLWhitScript.sql')
$oMYSQLCommand.CommandText = $sql
write-host $sql
try
{
$iRowsAffected=$oMYSQLCommand.executeNonQuery()

 }
 catch
{
    write-warning ("ERROR occured while ExecuteNonQuery")

}
# Do some Inserts or updates here and commit your changes

finally
{

$oConnection.Close()
    write-host "Closing Connection"
}