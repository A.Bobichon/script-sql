use woofing;

DROP TRIGGER if exists after_insert_lieu;

DELIMITER //
CREATE TRIGGER after_insert_lieu AFTER INSERT ON lieu 
FOR EACH ROW
BEGIN
	IF NEW.id < 50 THEN
		INSERT INTO woofing.hote( idlieu ) values ( NEW.id );
    END IF;
END;
//