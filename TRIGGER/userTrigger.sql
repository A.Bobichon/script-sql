use woofing;

DROP TRIGGER if exists after_insert_user;

DELIMITER //
CREATE TRIGGER after_insert_user AFTER INSERT ON lieu 
FOR EACH ROW
BEGIN
	IF NEW.id < 50 THEN
		INSERT INTO woofing.hote( idUser, idlieu ) values ( NEW.id, NEW.id );
    END IF;
END;
//