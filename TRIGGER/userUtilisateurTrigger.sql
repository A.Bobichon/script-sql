use woofing;

DROP TRIGGER if exists after_insert_user_utilisateur;

DELIMITER //
CREATE TRIGGER after_insert_user_utilisateur AFTER INSERT ON user 
FOR EACH ROW
BEGIN
	IF NEW.id > 50 THEN
		INSERT INTO woofing.utilisateur( idUser ) values ( NEW.id );
    END IF;
END;
//