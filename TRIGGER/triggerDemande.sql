
use woofing;

DROP TRIGGER if exists after_insert_demande;

DELIMITER //
CREATE TRIGGER after_insert_demande AFTER INSERT ON user 
FOR EACH ROW
	BEGIN
		IF NEW.id > 50 THEN	
		INSERT INTO woofing.demand	e( validation, idUtilisateur, idHote ) 
        	values ( FLOOR(RAND()*2), NEW.id, FLOOR(RAND()*50+1) );
    END IF;
END;
//
